package com.fcesar.curso.sboot.domain;

import java.io.Serializable;

import javax.persistence.*;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class AbstractEntity <ID extends Serializable> implements Serializable{

	@Id @GeneratedValue(strategy =  GenerationType.IDENTITY)
	private ID id;
	//Variável acima é do tipo genérico, ao qual se criou na assinatura da classe, ou seja, a variável chamada "id" será
	// do tipo "ID", tipo esse genárico, que permitirá a substituição dele.
	//O GenerationValue com o strategy = Identity é o ideal para se trabalhar em bancos MySQL
	
	

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractEntity<?> other = (AbstractEntity<?>) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "id=" + id;
	}
	
	
}
