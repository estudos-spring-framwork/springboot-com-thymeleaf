package com.fcesar.curso.sboot.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fcesar.curso.sboot.Service.CargoService;
import com.fcesar.curso.sboot.domain.Cargo;

@Controller
@RequestMapping("/cargos")
public class CargoController {
	
	@Autowired
	private CargoService service;
	
	@GetMapping("/cadastrar")
	public String cadastrar(Cargo cargo) {
		return "/cargo/cadastro";
	}
	
	@GetMapping("/listar")
	public String listar() {
		return "/cargo/lista";
	}

	@PostMapping("/salvar")
	public String salvar(Cargo cargo) {
		service.salvar(cargo);
		return "redirect:/cargos/cadastrar";
	}

}
