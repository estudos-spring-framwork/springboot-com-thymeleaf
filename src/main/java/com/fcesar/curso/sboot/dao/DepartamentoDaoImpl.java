package com.fcesar.curso.sboot.dao;

import org.springframework.stereotype.Repository;

import com.fcesar.curso.sboot.domain.Departamento;

@Repository
public class DepartamentoDaoImpl extends AbstractDao<Departamento, Long> implements DepartamentoDao {

}
