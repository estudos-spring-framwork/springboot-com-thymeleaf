package com.fcesar.curso.sboot.dao;

import org.springframework.stereotype.Repository;

import com.fcesar.curso.sboot.domain.Cargo;

@Repository
public class CargoDaoImpl extends AbstractDao<Cargo, Long> implements CargoDao {

}
