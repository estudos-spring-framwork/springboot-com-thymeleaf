package com.fcesar.curso.sboot.dao;

import org.springframework.stereotype.Repository;

import com.fcesar.curso.sboot.domain.Funcionario;

@Repository
public class FuncionarioDaoImpl extends AbstractDao<Funcionario, Long> implements FuncionarioDao {

}
