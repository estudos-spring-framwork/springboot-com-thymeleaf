package com.fcesar.curso.sboot.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fcesar.curso.sboot.dao.CargoDao;
import com.fcesar.curso.sboot.domain.Cargo;

@Service
public class CargoServiceImpl implements CargoService {
	
	@Autowired
	private CargoDao dao;

	@Transactional(readOnly = false)
	@Override
	public void salvar(Cargo cargo) {
		dao.save(cargo);
		
	}

	@Transactional(readOnly = false)
	@Override
	public void editar(Cargo cargo) {
		dao.update(cargo);
		
	}

	@Transactional(readOnly = false)
	@Override
	public void excluir(Long id) {
		dao.delete(id);
		
	}

	@Transactional(readOnly = true)
	@Override
	public Cargo buscarPorId(Long id) {
		
		return dao.findById(id);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Cargo> buscarTodos() {
		
		return dao.findAll();
	}

}
